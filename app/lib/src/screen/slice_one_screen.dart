import 'package:app/src/view_model/slice_one_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

// calculator
class SliceOneScreen extends StatelessWidget {
  const SliceOneScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SliceViewModel>.reactive(
      viewModelBuilder: () => SliceViewModel(),
      onModelReady: (model) => model.initialize(context),
      builder: (context, model, child){
        return Scaffold(
          body: Stack(
            children: [
              Container(
                color: Colors.greenAccent,
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: model.screenSize.height*0.45,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("asset/pict/4692523.jpg"),
                      fit: BoxFit.fill,
                      
                    ),
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(60), bottomRight: Radius.circular(60))
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: model.screenSize.height*0.05,
                  color: Colors.blue,
                ),
              ),
              Align(
                child: Container(
                  height: model.screenSize.height*0.7,
                  width: model.screenSize.width,
                  margin: EdgeInsets.only(top: model.screenSize.height*0.48, left: model.screenSize.width*0.05, right: model.screenSize.width*0.05),
                  child: 
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Skuy App", textAlign: TextAlign.left, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white, fontSize: model.screenSize.width*0.05),),
                          SizedBox(height: model.screenSize.height*0.01),
                          Flexible(child: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.", style: TextStyle(fontWeight: FontWeight.w400,color: Colors.white, fontSize: model.screenSize.width*0.04),),)
                        ],
                      )
                ),
              )
            ],
          ),
        );
      },
    );
  }
}