import 'package:app/src/core/app_config.dart';
import 'package:app/src/view_model/slice_chat_login_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class SliceChatLoginScreen extends StatelessWidget {
  const SliceChatLoginScreen({ Key? key }) : super(key: key);

  Widget generateTextField(SliceChatLoginViewModel model,String label, bool secureText, TextEditingController controller, TextInputType type){
    return Container(
      width: model.screenSize.width*0.8,
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(label,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white
              ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            // height: model.screenSize.width*0.8,
            child: TextField(
              keyboardType: type,
              controller: controller,
              obscureText: secureText,
              obscuringCharacter: "*",
              decoration: InputDecoration(
                isDense: true,                      // Added this
                contentPadding: EdgeInsets.all(15),  
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  borderSide: BorderSide.none,
                ),
                fillColor: Colors.white,
                filled: true                            
                ),
            ),
          )
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SliceChatLoginViewModel>.reactive(
      viewModelBuilder: () => SliceChatLoginViewModel(),
      onModelReady: (model) => model.initialize(context),
      builder: (context, model, child){
        return Scaffold(
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: AppConfig.BACKGROUND_CHAT_GRADIENT_COLOR,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    // color: Colors.grey,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text("LOGIN",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: model.screenSize.width*0.09
                          ),
                        ),
                      ),
                      SizedBox(height: 100,),
                      generateTextField(model,"E-MAIL ADDRESS", false, model.emailController, TextInputType.emailAddress),
                      SizedBox(height: 30,),
                      generateTextField(model,"PASSWORD", true, model.passwordController, TextInputType.text),
                      SizedBox(height: 30,),
                      ElevatedButton(
                        onPressed: model.loginProcess,
                        child: Container(
                          alignment: Alignment.center,
                          width: model.screenSize.width*0.5,
                          height: model.screenSize.height*0.06,
                          child: Text("LOGIN",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: model.screenSize.width*0.03
                            ),
                          ),
                        ),
                        
                        style: ButtonStyle(
                          shadowColor: MaterialStateProperty.all<Color>(Colors.pink),
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.pink),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)
                            )
                          )
                        ),
                      )
                    ],
                  )
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}