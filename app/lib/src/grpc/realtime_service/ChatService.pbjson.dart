///
//  Generated code. Do not modify.
//  source: ChatService.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use createRoomRequestDescriptor instead')
const CreateRoomRequest$json = const {
  '1': 'CreateRoomRequest',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'creatorUserId', '3': 2, '4': 1, '5': 9, '10': 'creatorUserId'},
  ],
};

/// Descriptor for `CreateRoomRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createRoomRequestDescriptor = $convert.base64Decode('ChFDcmVhdGVSb29tUmVxdWVzdBISCgRuYW1lGAEgASgJUgRuYW1lEiQKDWNyZWF0b3JVc2VySWQYAiABKAlSDWNyZWF0b3JVc2VySWQ=');
@$core.Deprecated('Use createRoomResponseDescriptor instead')
const CreateRoomResponse$json = const {
  '1': 'CreateRoomResponse',
  '2': const [
    const {'1': 'statusCode', '3': 1, '4': 1, '5': 9, '10': 'statusCode'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'success', '3': 3, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'errorMessage', '3': 4, '4': 1, '5': 9, '10': 'errorMessage'},
    const {'1': 'data', '3': 5, '4': 1, '5': 11, '6': '.ChatService.ChatRoomModel', '10': 'data'},
  ],
};

/// Descriptor for `CreateRoomResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createRoomResponseDescriptor = $convert.base64Decode('ChJDcmVhdGVSb29tUmVzcG9uc2USHgoKc3RhdHVzQ29kZRgBIAEoCVIKc3RhdHVzQ29kZRIYCgdtZXNzYWdlGAIgASgJUgdtZXNzYWdlEhgKB3N1Y2Nlc3MYAyABKAhSB3N1Y2Nlc3MSIgoMZXJyb3JNZXNzYWdlGAQgASgJUgxlcnJvck1lc3NhZ2USLgoEZGF0YRgFIAEoCzIaLkNoYXRTZXJ2aWNlLkNoYXRSb29tTW9kZWxSBGRhdGE=');
@$core.Deprecated('Use chatRoomModelDescriptor instead')
const ChatRoomModel$json = const {
  '1': 'ChatRoomModel',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'code', '3': 3, '4': 1, '5': 9, '10': 'code'},
    const {'1': 'creatorUserId', '3': 4, '4': 1, '5': 9, '10': 'creatorUserId'},
  ],
};

/// Descriptor for `ChatRoomModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chatRoomModelDescriptor = $convert.base64Decode('Cg1DaGF0Um9vbU1vZGVsEg4KAmlkGAEgASgJUgJpZBISCgRuYW1lGAIgASgJUgRuYW1lEhIKBGNvZGUYAyABKAlSBGNvZGUSJAoNY3JlYXRvclVzZXJJZBgEIAEoCVINY3JlYXRvclVzZXJJZA==');
@$core.Deprecated('Use joinRoomRequestDescriptor instead')
const JoinRoomRequest$json = const {
  '1': 'JoinRoomRequest',
  '2': const [
    const {'1': 'roomCode', '3': 1, '4': 1, '5': 9, '10': 'roomCode'},
    const {'1': 'memberUserId', '3': 2, '4': 1, '5': 9, '10': 'memberUserId'},
  ],
};

/// Descriptor for `JoinRoomRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinRoomRequestDescriptor = $convert.base64Decode('Cg9Kb2luUm9vbVJlcXVlc3QSGgoIcm9vbUNvZGUYASABKAlSCHJvb21Db2RlEiIKDG1lbWJlclVzZXJJZBgCIAEoCVIMbWVtYmVyVXNlcklk');
@$core.Deprecated('Use joinRoomResponseDescriptor instead')
const JoinRoomResponse$json = const {
  '1': 'JoinRoomResponse',
  '2': const [
    const {'1': 'statusCode', '3': 1, '4': 1, '5': 9, '10': 'statusCode'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'success', '3': 3, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'errorMessage', '3': 4, '4': 1, '5': 9, '10': 'errorMessage'},
    const {'1': 'data', '3': 5, '4': 1, '5': 11, '6': '.ChatService.ChatRoomModel', '10': 'data'},
  ],
};

/// Descriptor for `JoinRoomResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinRoomResponseDescriptor = $convert.base64Decode('ChBKb2luUm9vbVJlc3BvbnNlEh4KCnN0YXR1c0NvZGUYASABKAlSCnN0YXR1c0NvZGUSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIYCgdzdWNjZXNzGAMgASgIUgdzdWNjZXNzEiIKDGVycm9yTWVzc2FnZRgEIAEoCVIMZXJyb3JNZXNzYWdlEi4KBGRhdGEYBSABKAsyGi5DaGF0U2VydmljZS5DaGF0Um9vbU1vZGVsUgRkYXRh');
@$core.Deprecated('Use sendMessageRequestDescriptor instead')
const SendMessageRequest$json = const {
  '1': 'SendMessageRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 9, '10': 'roomId'},
    const {'1': 'clientId', '3': 2, '4': 1, '5': 9, '10': 'clientId'},
    const {'1': 'userId', '3': 3, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'message', '3': 4, '4': 1, '5': 9, '10': 'message'},
  ],
};

/// Descriptor for `SendMessageRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendMessageRequestDescriptor = $convert.base64Decode('ChJTZW5kTWVzc2FnZVJlcXVlc3QSFgoGcm9vbUlkGAEgASgJUgZyb29tSWQSGgoIY2xpZW50SWQYAiABKAlSCGNsaWVudElkEhYKBnVzZXJJZBgDIAEoCVIGdXNlcklkEhgKB21lc3NhZ2UYBCABKAlSB21lc3NhZ2U=');
@$core.Deprecated('Use sendMessageResponseDescriptor instead')
const SendMessageResponse$json = const {
  '1': 'SendMessageResponse',
  '2': const [
    const {'1': 'statusCode', '3': 1, '4': 1, '5': 9, '10': 'statusCode'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'success', '3': 3, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'errorMessage', '3': 4, '4': 1, '5': 9, '10': 'errorMessage'},
    const {'1': 'data', '3': 5, '4': 1, '5': 11, '6': '.ChatService.ChatRoomMessage', '10': 'data'},
  ],
};

/// Descriptor for `SendMessageResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendMessageResponseDescriptor = $convert.base64Decode('ChNTZW5kTWVzc2FnZVJlc3BvbnNlEh4KCnN0YXR1c0NvZGUYASABKAlSCnN0YXR1c0NvZGUSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIYCgdzdWNjZXNzGAMgASgIUgdzdWNjZXNzEiIKDGVycm9yTWVzc2FnZRgEIAEoCVIMZXJyb3JNZXNzYWdlEjAKBGRhdGEYBSABKAsyHC5DaGF0U2VydmljZS5DaGF0Um9vbU1lc3NhZ2VSBGRhdGE=');
@$core.Deprecated('Use chatRoomMessageDescriptor instead')
const ChatRoomMessage$json = const {
  '1': 'ChatRoomMessage',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 9, '10': 'roomId'},
    const {'1': 'messages', '3': 2, '4': 3, '5': 11, '6': '.ChatService.MessageModel', '10': 'messages'},
  ],
};

/// Descriptor for `ChatRoomMessage`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List chatRoomMessageDescriptor = $convert.base64Decode('Cg9DaGF0Um9vbU1lc3NhZ2USFgoGcm9vbUlkGAEgASgJUgZyb29tSWQSNQoIbWVzc2FnZXMYAiADKAsyGS5DaGF0U2VydmljZS5NZXNzYWdlTW9kZWxSCG1lc3NhZ2Vz');
@$core.Deprecated('Use messageModelDescriptor instead')
const MessageModel$json = const {
  '1': 'MessageModel',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 9, '10': 'userId'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'createdAt', '3': 3, '4': 1, '5': 9, '10': 'createdAt'},
    const {'1': 'updatedAt', '3': 4, '4': 1, '5': 9, '10': 'updatedAt'},
    const {'1': 'deletedAt', '3': 5, '4': 1, '5': 9, '10': 'deletedAt'},
  ],
};

/// Descriptor for `MessageModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageModelDescriptor = $convert.base64Decode('CgxNZXNzYWdlTW9kZWwSFgoGdXNlcklkGAEgASgJUgZ1c2VySWQSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIcCgljcmVhdGVkQXQYAyABKAlSCWNyZWF0ZWRBdBIcCgl1cGRhdGVkQXQYBCABKAlSCXVwZGF0ZWRBdBIcCglkZWxldGVkQXQYBSABKAlSCWRlbGV0ZWRBdA==');
