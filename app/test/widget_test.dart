// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:app/src/utility/crypto_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'dart:io';
import 'package:app/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });

  testWidgets("test yield", (WidgetTester tester) async {
    Stream<int> res = await tyAsync();
    res.every((d) {
      if(d > 4){
        return false;
      }
      print(d);
      return true;
    });
    print("");
  });

  testWidgets("test crypt", (WidgetTester tester) async {
    String res =  CryptoUtil.encryptAesCBC("dika", "*e-EncryptionKey", "1-frgjkhltiofpc*");
    print(res);
  });

}

 Iterable<int> tySync() sync*{
   var a = [1, 2, 3, 4, 5];

  for (var e in a) {
    if (e > 2) yield e;
  }
}

 Stream<int> tyAsync() async*{
   var a = [1, 2, 3, 4, 5];

  for (var e in a) {
    if (e > 2) yield e;
  }
}
