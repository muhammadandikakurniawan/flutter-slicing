import 'package:flutter/material.dart';

class AppConfig {
  static const String SIGNAL_R_SERVER_ADDRESS = "http://192.168.0.19:5100";
  // static const String SIGNAL_R_SERVER_ADDRESS = "http://0e89-139-193-220-5.ngrok.io";

  static const String AES_SECRET_KEY = "aesEncryptionKey";
  static const String AES_INITIAL_VECTOR = "encryptionIntVec";

  static const String GRPC_STORAGE_SERVICE_APP_KEY = "storage-service-key";
  static const String GRPC_STORAGE_SERVICE_HOST = "192.168.0.19";
  static const int GRPC_STORAGE_SERVICE_PORT = 4042;
  static const int GRPC_STORAGE_SERVICE_TIMEOUT = 5;

  static const String GRPC_USER_SERVICE_APP_KEY = "user-service-key";
  static const String GRPC_USER_SERVICE_HOST = "192.168.0.19";
  static const int GRPC_USER_SERVICE_PORT = 4045;
  static const int GRPC_USER_SERVICE_TIMEOUT = 5;

  static const String GRPC_REALTIME_SERVICE_APP_KEY = "realtime-service-key";
  static const String GRPC_REALTIME_SERVICE_HOST = "192.168.0.19";
  static const int GRPC_REALTIME_SERVICE_PORT = 2000;
  static const int GRPC_REALTIME_SERVICE_TIMEOUT = 5;

  // static const String GRPC_REALTIME_SERVICE_APP_KEY = "realtime-service-key";
  // static const String GRPC_REALTIME_SERVICE_HOST = "0.tcp.ngrok.io";
  // static const int GRPC_REALTIME_SERVICE_PORT = 17160;
  // static const int GRPC_REALTIME_SERVICE_TIMEOUT = 5;

  static final LinearGradient BACKGROUND_CHAT_GRADIENT_COLOR = LinearGradient(begin: Alignment.bottomCenter,end: Alignment.topCenter,colors: [Colors.blue.shade500, Colors.purple, Colors.pink]);

}