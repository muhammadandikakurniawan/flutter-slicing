import 'package:signalr_netcore/signalr_client.dart';

class SignalRHelper{

  late String _serverUrl;
  late HubConnection _hubConnection;
  late String _clientId;

  String get clientId => _clientId;

  SignalRHelper(String serverUrl){
    _serverUrl = serverUrl;
    _hubConnection = HubConnectionBuilder()
    .withUrl(_serverUrl,).build() ;

    
  }

  connect() async {
    await _hubConnection.start();
  }

  onConnect(Function(String) act){
    _hubConnection.on("connected",(connectionId) async {
      if(connectionId != null){
        _clientId = connectionId[0].toString();
        act(_clientId);
      }
    });
  }

  onConnecting(Function(Exception? arg) act){
    _hubConnection.onreconnecting(({error}) {
      act(error);
    });
  }

  onDisconnect(Function(Exception?) act){
    _hubConnection.onclose(({error}) {
      act(error);
    });
  }

  on(String methodName, Function(List<Object>?) act){
    _hubConnection.on(methodName, (arguments) { 
      act(arguments);
    });
  }

  send(String methodName,{List<Object>? args}) async {
    await _hubConnection.invoke(methodName, args: args);
  }

}