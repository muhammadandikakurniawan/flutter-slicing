import 'package:app/src/core/app_config.dart';
import 'package:app/src/view_model/slice_chat_activation_account_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class SliceChatActivationAccountScreen extends StatelessWidget {
  const SliceChatActivationAccountScreen({ Key? key }) : super(key: key);

  Widget generateTextField(SliceChatActivationAccountViewModel model,String label, bool secureText, TextEditingController controller, TextInputType type){
    return Container(
      width: model.screenSize.width*0.8,
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(label,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white
              ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            // height: model.screenSize.width*0.8,
            child: TextField(
              textAlign: TextAlign.center,
              maxLength: 7,
              keyboardType: type,
              controller: controller,
              obscureText: secureText,
              obscuringCharacter: "*",
              style: TextStyle(
                fontSize: model.screenSize.width*0.08
              ),
              decoration: InputDecoration(
                isDense: true,                      // Added this
                contentPadding: EdgeInsets.all(15),  
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  borderSide: BorderSide.none,
                ),
                fillColor: Colors.white,
                filled: true                            
                ),
            ),
          )
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SliceChatActivationAccountViewModel>.reactive(
      viewModelBuilder: () => SliceChatActivationAccountViewModel(),
      onModelReady: (model) => model.initialize(context),
      builder: (context, model, child){
        return Scaffold(
          appBar: AppBar(backgroundColor: Colors.pink,),
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: AppConfig.BACKGROUND_CHAT_GRADIENT_COLOR,
                ),
              ),

              Align(
                alignment: Alignment.center,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    // color: Colors.grey,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text("Please check your email \nand input the activation code",
                        textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: model.screenSize.width*0.04
                          ),
                        ),
                      ),
                      SizedBox(height: 60,),
                      generateTextField(model,"ACTIVATION CODE", false, model.inputCodeController, TextInputType.emailAddress),
                      SizedBox(height: 30,),
                      ElevatedButton(
                        onPressed: model.activateAccountProcess,
                        child: Container(
                          alignment: Alignment.center,
                          width: model.screenSize.width*0.5,
                          height: model.screenSize.height*0.06,
                          child: Text("SUBMIT",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: model.screenSize.width*0.03
                            ),
                          ),
                        ),
                        
                        style: ButtonStyle(
                          shadowColor: MaterialStateProperty.all<Color>(Colors.pink),
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.pink),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)
                            )
                          )
                        ),
                      )
                    ],
                  )
                ),
              ),

            ],
          ),
        );
      },
    );
  }
}