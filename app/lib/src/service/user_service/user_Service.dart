import 'package:app/src/core/app_config.dart';
import 'package:app/src/grpc/user_service/UserService.pbgrpc.dart';
import 'package:grpc/grpc.dart';

class UserService {
  late UserServiceClient _grpcClient;
  late ClientChannel _channel;

  UserService(){
    _channel = ClientChannel(AppConfig.GRPC_USER_SERVICE_HOST,port: AppConfig.GRPC_USER_SERVICE_PORT,options: const ChannelOptions(connectionTimeout: Duration(seconds: AppConfig.GRPC_USER_SERVICE_TIMEOUT),credentials: ChannelCredentials.insecure()));

    Map<String, String> meta = {
      "x-api-key": AppConfig.GRPC_USER_SERVICE_APP_KEY
    };

    _grpcClient = UserServiceClient(_channel, options: CallOptions(metadata: meta));
  }

  ResponseFuture<LoginResponse> login(String email, password){
    LoginRequest request = LoginRequest(email: email, password: password);
    return _grpcClient.login(request);
  }

  ResponseFuture<ActivateAccountResponse> activationAccount(String email, password, code){
    ActivateAccountRequest request = ActivateAccountRequest(email: email, password: password, activationCode: code);
    return _grpcClient.activateAccount(request);
  }

}