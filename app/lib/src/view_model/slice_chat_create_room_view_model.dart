import 'dart:convert';

import 'package:app/src/core/container.dart';
import 'package:app/src/model/UserModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked/stacked.dart';
import 'package:app/src/core/container.dart' as appContainer;

class SliceChatCreateRoomViewModel extends BaseViewModel {

  late BuildContext _context;
  late Size _screenSize;
  late SharedPreferences _localStorage;
  UserModel _userData = UserModel();
  TextEditingController _inputCodeController = new TextEditingController();

  Size get screenSize => _screenSize;
  TextEditingController get inputCodeController => _inputCodeController;
  UserModel get userData => _userData;

  initialize(BuildContext context) async{
    _context = context;
    _screenSize = MediaQuery.of(_context).size;
    _localStorage = await SharedPreferences.getInstance();

    String? userJson = _localStorage.getString("data_user_login");
    if( userJson != null){
      _userData.fromJson(userJson);
    }

    notifyListeners();
  }

}