///
//  Generated code. Do not modify.
//  source: UserService.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use loginRequestDescriptor instead')
const LoginRequest$json = const {
  '1': 'LoginRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `LoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginRequestDescriptor = $convert.base64Decode('CgxMb2dpblJlcXVlc3QSFAoFZW1haWwYASABKAlSBWVtYWlsEhoKCHBhc3N3b3JkGAIgASgJUghwYXNzd29yZA==');
@$core.Deprecated('Use loginResponseDescriptor instead')
const LoginResponse$json = const {
  '1': 'LoginResponse',
  '2': const [
    const {'1': 'statusCode', '3': 1, '4': 1, '5': 9, '10': 'statusCode'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'success', '3': 3, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'errorMessage', '3': 4, '4': 1, '5': 9, '10': 'errorMessage'},
    const {'1': 'data', '3': 5, '4': 1, '5': 11, '6': '.user_service.UserModel', '10': 'data'},
  ],
};

/// Descriptor for `LoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginResponseDescriptor = $convert.base64Decode('Cg1Mb2dpblJlc3BvbnNlEh4KCnN0YXR1c0NvZGUYASABKAlSCnN0YXR1c0NvZGUSGAoHbWVzc2FnZRgCIAEoCVIHbWVzc2FnZRIYCgdzdWNjZXNzGAMgASgIUgdzdWNjZXNzEiIKDGVycm9yTWVzc2FnZRgEIAEoCVIMZXJyb3JNZXNzYWdlEisKBGRhdGEYBSABKAsyFy51c2VyX3NlcnZpY2UuVXNlck1vZGVsUgRkYXRh');
@$core.Deprecated('Use activateAccountRequestDescriptor instead')
const ActivateAccountRequest$json = const {
  '1': 'ActivateAccountRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'activationCode', '3': 3, '4': 1, '5': 9, '10': 'activationCode'},
  ],
};

/// Descriptor for `ActivateAccountRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List activateAccountRequestDescriptor = $convert.base64Decode('ChZBY3RpdmF0ZUFjY291bnRSZXF1ZXN0EhQKBWVtYWlsGAEgASgJUgVlbWFpbBIaCghwYXNzd29yZBgCIAEoCVIIcGFzc3dvcmQSJgoOYWN0aXZhdGlvbkNvZGUYAyABKAlSDmFjdGl2YXRpb25Db2Rl');
@$core.Deprecated('Use activateAccountResponseDescriptor instead')
const ActivateAccountResponse$json = const {
  '1': 'ActivateAccountResponse',
  '2': const [
    const {'1': 'statusCode', '3': 1, '4': 1, '5': 9, '10': 'statusCode'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'success', '3': 3, '4': 1, '5': 8, '10': 'success'},
    const {'1': 'errorMessage', '3': 4, '4': 1, '5': 9, '10': 'errorMessage'},
    const {'1': 'data', '3': 5, '4': 1, '5': 11, '6': '.user_service.UserModel', '10': 'data'},
  ],
};

/// Descriptor for `ActivateAccountResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List activateAccountResponseDescriptor = $convert.base64Decode('ChdBY3RpdmF0ZUFjY291bnRSZXNwb25zZRIeCgpzdGF0dXNDb2RlGAEgASgJUgpzdGF0dXNDb2RlEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USGAoHc3VjY2VzcxgDIAEoCFIHc3VjY2VzcxIiCgxlcnJvck1lc3NhZ2UYBCABKAlSDGVycm9yTWVzc2FnZRIrCgRkYXRhGAUgASgLMhcudXNlcl9zZXJ2aWNlLlVzZXJNb2RlbFIEZGF0YQ==');
@$core.Deprecated('Use userModelDescriptor instead')
const UserModel$json = const {
  '1': 'UserModel',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'username', '3': 2, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'email', '3': 3, '4': 1, '5': 9, '10': 'email'},
  ],
};

/// Descriptor for `UserModel`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userModelDescriptor = $convert.base64Decode('CglVc2VyTW9kZWwSDgoCaWQYASABKAlSAmlkEhoKCHVzZXJuYW1lGAIgASgJUgh1c2VybmFtZRIUCgVlbWFpbBgDIAEoCVIFZW1haWw=');
