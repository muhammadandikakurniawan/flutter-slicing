import 'dart:typed_data';
import "package:pointycastle/export.dart";
import 'package:convert/convert.dart';
import 'dart:convert';

class CryptoUtil {
  static String encryptAesCBC(String plainStr, String keyStr, String ivStr) {
    var key = Uint8List.fromList(utf8.encode(keyStr));
    var iv = Uint8List.fromList(utf8.encode(ivStr));
    var plainTxt = Uint8List.fromList(utf8.encode(plainStr));

    
    CBCBlockCipher cipher = new CBCBlockCipher(new AESFastEngine());
    ParametersWithIV<KeyParameter> params = new ParametersWithIV<KeyParameter>(new KeyParameter(key), iv);
    PaddedBlockCipherParameters<ParametersWithIV<KeyParameter>, Null> paddingParams = new PaddedBlockCipherParameters<ParametersWithIV<KeyParameter>, Null>(params, null);
    PaddedBlockCipherImpl paddingCipher = new PaddedBlockCipherImpl(new PKCS7Padding(), cipher);
    paddingCipher.init(true, paddingParams);
    return base64Encode(paddingCipher.process(plainTxt));
  }
}