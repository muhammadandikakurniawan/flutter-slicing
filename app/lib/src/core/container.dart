import 'package:app/src/service/realtime_service/chat_service.dart' as realtime_chat_service;
import 'package:app/src/service/storage_service/chat_service.dart';
import 'package:app/src/service/user_service/user_Service.dart';

class Container {
    static final ChatService chatService = ChatService();
    static final UserService userService = UserService();
    static final realtime_chat_service.ChatService realtimeChatService = realtime_chat_service.ChatService();
}