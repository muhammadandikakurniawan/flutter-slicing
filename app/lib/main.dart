import 'package:app/src/screen/slice_chat_login_screen.dart';
import 'package:app/src/screen/slice_chat_screen.dart';
import 'package:app/src/screen/slice_one_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SliceChatLoginScreen(),
    );
  }
}