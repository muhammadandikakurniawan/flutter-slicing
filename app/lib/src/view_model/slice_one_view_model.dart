import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class SliceViewModel extends BaseViewModel{

    late BuildContext _context;
    late Size _screenSize;

    Size get screenSize => _screenSize;

    initialize(BuildContext context) async{
      _context = context;
      _screenSize = MediaQuery.of(_context).size;
      notifyListeners();
    }

}