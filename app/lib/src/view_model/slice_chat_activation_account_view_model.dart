import 'dart:convert';

import 'package:app/src/core/container.dart';
import 'package:app/src/grpc/user_service/UserService.pb.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked/stacked.dart';
import 'package:app/src/core/container.dart' as appContainer;

class SliceChatActivationAccountViewModel extends BaseViewModel {

  late BuildContext _context;
  late Size _screenSize;
  late SharedPreferences _localStorage;
  TextEditingController _inputCodeController = new TextEditingController();

  Size get screenSize => _screenSize;
  TextEditingController get inputCodeController => _inputCodeController;

  initialize(BuildContext context) async{
    _context = context;
    _screenSize = MediaQuery.of(_context).size;
    _localStorage = await SharedPreferences.getInstance();

    notifyListeners();
  }

  activateAccountProcess() async {
    Map<String, dynamic> dataUserLogin = {};
    if(_localStorage.getString("data_user_login") != null){
      dataUserLogin = jsonDecode(_localStorage.getString("data_user_login")!);
    }
    String email = dataUserLogin.containsKey("email") ? dataUserLogin["email"] : "";
    String password = dataUserLogin.containsKey("password") ? dataUserLogin["password"] : "";
    String code = _inputCodeController.text;
    ActivateAccountResponse response = await appContainer.Container.userService.activationAccount(email, password, code);
  }

}