import 'dart:io';
import 'dart:async';
import 'package:app/src/core/app_config.dart';
import 'package:app/src/grpc/realtime_service/ChatService.pbgrpc.dart';
import 'package:chunked_stream/chunked_stream.dart';
import 'package:grpc/grpc.dart';
import 'package:path/path.dart';

class ChatService {

  late ChatServiceClient _grpcClient;
  late ClientChannel _channel;

  ChatService(){
    _channel = ClientChannel(AppConfig.GRPC_REALTIME_SERVICE_HOST,port: AppConfig.GRPC_REALTIME_SERVICE_PORT,options: const ChannelOptions(connectionTimeout: Duration(seconds: AppConfig.GRPC_REALTIME_SERVICE_TIMEOUT),credentials: ChannelCredentials.insecure()));

    Map<String, String> meta = {
      "x-api-key": AppConfig.GRPC_REALTIME_SERVICE_APP_KEY
    };

    _grpcClient = ChatServiceClient(_channel, options: CallOptions(metadata: meta));
  }


    Future<CreateRoomResponse> createRoom(String roomName, userId) async{
      CreateRoomRequest request= new CreateRoomRequest(name: roomName, creatorUserId: userId);

      CreateRoomResponse response = await _grpcClient.createRoom(request);

      return response;
    }

    Future<JoinRoomResponse> joinRoom(String roomCode, userId) async{
      JoinRoomRequest request= new JoinRoomRequest(roomCode: roomCode, memberUserId: userId);

      JoinRoomResponse response = await _grpcClient.joinRoom(request);

      return response;
    }

    Future<SendMessageResponse> sendMessage(String roomId, clientId, userId, msg) async{
      SendMessageRequest request = SendMessageRequest(roomId: roomId, clientId: clientId, userId: userId, message: msg);
      SendMessageResponse response = await _grpcClient.sendMessage(request);
      return response;
    }

}