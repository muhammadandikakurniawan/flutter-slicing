import 'dart:convert';

class ChatRoomModel {
  String id;
  String name;
  String code;
  String creatorUserId;
  List<String> membersUserId = [];

  ChatRoomModel({this.id = "", this.name = "", this.code = "", this.creatorUserId = "", required this.membersUserId}){

  }

  fromJson(String json){
    Map<String, dynamic> dataJson = jsonDecode(json);

    id = dataJson.containsKey("id") ? dataJson["id"] : "";
    name = dataJson.containsKey("name") ? dataJson["name"] : "";
    code = dataJson.containsKey("code") ? dataJson["code"] : "";
    creatorUserId = dataJson.containsKey("creatorUserId") ? dataJson["creatorUserId"] : "";
    membersUserId = dataJson.containsKey("membersUserId") ? dataJson["membersUserId"] : [];
  }

  String toJson(){
    Map<String, dynamic> data = {
      "id": id,
      "name": name,
      "code": code,
      "creatorUserId": creatorUserId,
      "membersUserId": membersUserId
    };

    return JsonEncoder().convert(data);
  }

}