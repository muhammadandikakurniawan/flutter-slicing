import 'dart:convert';

import 'package:app/src/core/app_config.dart';
import 'package:app/src/core/container.dart';
import 'package:app/src/model/UserModel.dart';
import 'package:app/src/screen/slice_chat_activation_account_screen.dart';
import 'package:app/src/screen/slice_chat_optional_form_screen.dart';
import 'package:app/src/utility/crypto_util.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:app/src/core/container.dart' as appContainer;
import 'package:shared_preferences/shared_preferences.dart';

class SliceChatLoginViewModel extends BaseViewModel {

  late BuildContext _context;
  late Size _screenSize;
  late SharedPreferences _localStorage;
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  

  Size get screenSize => _screenSize;
  TextEditingController get emailController => _emailController;
  TextEditingController get passwordController => _passwordController;

  initialize(BuildContext context) async{
    _context = context;
    _screenSize = MediaQuery.of(_context).size;
    _localStorage = await SharedPreferences.getInstance();

    if(_localStorage.get("data_user_login") != null){
      Navigator.pushReplacement(_context, 
        MaterialPageRoute(builder: (context) => SliceChatOptionalFormScreen())
      );
    }
    notifyListeners();
  }

  loginProcess() async {
    String email = _emailController.text;
    String password = CryptoUtil.encryptAesCBC(_passwordController.text, AppConfig.AES_SECRET_KEY, AppConfig.AES_INITIAL_VECTOR);
    var response = await appContainer.Container.userService.login(email, password);

    UserModel dataUserLogin = UserModel(email: email, password: password);
    if(response.statusCode == "500"){
      print(response);
      return;
    }

    switch(response.statusCode){
      case "10":
        dataUserLogin.username = response.data.username;
        Navigator.push(_context, 
          MaterialPageRoute(builder: (context) => SliceChatActivationAccountScreen())
        );
        break;
      case "200":
        dataUserLogin.id = response.data.id;
        dataUserLogin.username = response.data.username;
        _localStorage.setString("data_user_login", dataUserLogin.toJson());
        Navigator.pushReplacement(_context, 
          MaterialPageRoute(builder: (context) => SliceChatOptionalFormScreen())
        );
        
        break;
    }
  }

}