import 'package:app/src/core/app_config.dart';
import 'package:app/src/view_model/slice_chat_activation_account_view_model.dart';
import 'package:app/src/view_model/slice_chat_optional_form_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';

class SliceChatOptionalFormScreen extends StatelessWidget {
  SliceChatOptionalFormScreen({ Key? key }) : super(key: key);

  Widget generateButton(SliceChatOptionalFormViewModel model, String txt, Function() onPress, Color color){
    return ElevatedButton(
      onPressed: onPress,
      child: Container(
        alignment: Alignment.center,
        width: model.screenSize.width*0.6,
        height: model.screenSize.height*0.08,
        child: Text(txt,
          style: TextStyle(
            color: Colors.white,
            fontSize: model.screenSize.width*0.03
          ),
        ),
      ),
      style: ButtonStyle(
        shadowColor: MaterialStateProperty.all<Color>(color),
        backgroundColor: MaterialStateProperty.all<Color>(color),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
          )
        )
      ),
    );
  }

  Widget generateTextField(SliceChatOptionalFormViewModel model,String label, bool secureText, TextEditingController controller, TextInputType type){
    return Container(
      width: model.screenSize.width*0.8,
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(label,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white
              ),
            ),
          ),
          SizedBox(height: 5,),
          Container(
            // height: model.screenSize.width*0.8,
            child: TextField(
              textAlign: TextAlign.center,
              keyboardType: type,
              controller: controller,
              obscureText: secureText,
              obscuringCharacter: "*",
              style: TextStyle(
                fontSize: model.screenSize.width*0.08
              ),
              decoration: InputDecoration(
                isDense: true,                      // Added this
                contentPadding: EdgeInsets.all(15),  
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  borderSide: BorderSide.none,
                ),
                fillColor: Colors.white,
                filled: true                            
                ),
            ),
          ),
          
        ],
      )
    );
  }

  void showDialogInputRoom(SliceChatOptionalFormViewModel model, String buttonLabel, inputLabel, Function() action) {
    showAnimatedDialog(
      context: model.context,
      barrierDismissible: true,
      builder: (BuildContext dialogCtx) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            width: model.screenSize.width*0.9,
            height: model.screenSize.height*0.5,
            decoration: BoxDecoration(
              gradient: AppConfig.BACKGROUND_CHAT_GRADIENT_COLOR,
              borderRadius: BorderRadius.circular(60),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 6), // changes position of shadow
                ),
              ]
            ),
            child: Material(
              type: MaterialType.transparency,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: generateTextField(model, inputLabel, false, model.inputRoomNameController, TextInputType.multiline),
                  ),
                  SizedBox(height: 30,),
                  generateButton(model,buttonLabel, action, Colors.pink),
                  SizedBox(height: 30,),
                  generateButton(model,"CLOSE", Navigator.of(dialogCtx).pop, Colors.grey),
                ],
              ),
            )
          ),
        );
      },
      animationType: DialogTransitionType.slideFromBottom,
      curve: Curves.fastOutSlowIn,
      duration: Duration(milliseconds:500),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SliceChatOptionalFormViewModel>.reactive(
      viewModelBuilder: () => SliceChatOptionalFormViewModel(),
      onModelReady: (model) => model.initialize(context),
      builder: (context, model, child){
        return Scaffold(
          appBar: AppBar(backgroundColor: Colors.pink,),
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: AppConfig.BACKGROUND_CHAT_GRADIENT_COLOR,
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Welcome ",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: model.screenSize.width*0.05
                        ),
                      ),
                      Text(model.userData.email,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: model.screenSize.width*0.06
                        ),
                      ),
                    ],
                  )
                )
              ),
              Align(
                // alignment: Alignment.bottomCenter,
                child: Container(
                  alignment: Alignment.center,
                  width: model.screenSize.width*0.9,
                  height: model.screenSize.height*0.5,
                  margin: EdgeInsets.only(bottom: 10),
                  decoration: BoxDecoration(
                    // color: Colors.white,
                    borderRadius: BorderRadius.circular(60),
                    boxShadow: [
                      // BoxShadow(
                      //   color: Colors.grey.withOpacity(0.2),
                      //   spreadRadius: 5,
                      //   blurRadius: 7,
                      //   offset: Offset(0, 1), // changes position of shadow
                      // ),
                    ]
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      generateButton(model,"JOIN   ROOM", (){showDialogInputRoom(model, "JOIN", "ROOM CODE", model.joinRoom);}, Colors.pink),
                      SizedBox(height: 30,),
                      generateButton(model,"CREATE   NEW   ROOM", (){showDialogInputRoom(model, "CREATE", "ROOM NAME", model.createChatRoom);}, Colors.pink),
                    ],
                  )
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}