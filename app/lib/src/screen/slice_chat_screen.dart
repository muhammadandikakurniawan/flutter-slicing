import 'dart:io';
import 'dart:math';

import 'package:app/src/core/app_config.dart';
import 'package:app/src/view_model/slice_chat_view_model.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:circular_menu/circular_menu.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stacked/stacked.dart';

class SliceChatScreen extends StatelessWidget {
  SliceChatScreen({ Key? key }) : super(key: key);

  Widget buildChatElementWidget(SliceChatViewModel model, BuildContext context, int index){

      // int lenData = model.listChat.length;
      // int idx = (lenData-1) - index;

      String chatTxt = model.listChat[index];
      var alignment = Alignment.centerLeft;
      var color = Colors.white;
      var borderRadius = const BorderRadius.only(topRight: Radius.circular(10), bottomLeft: Radius.circular(10), bottomRight: Radius.circular(1));
      
      if(index%2 == 0){
        alignment = Alignment.centerRight;
        color = const Color.fromRGBO(131, 210, 252, 1);
        borderRadius = const BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(1), bottomRight: Radius.circular(10));
      }

      return Container(
        margin: EdgeInsets.only(top: model.screenSize.height*0.04),
        alignment: alignment,
        child: Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          constraints: BoxConstraints( maxWidth: model.screenSize.width*0.5),
          padding: const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
          child: Column(
            children: [
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.blue,
                      radius: model.screenSize.width*0.04,
                  ),
                  SizedBox(width: 5,),
                  Container(
                    child:Text("user"+index.toString(), textAlign: TextAlign.left, style: const TextStyle(fontWeight: FontWeight.w500),),
                  )
                ],
              ),
              
              SizedBox(height: 10,),
              Align(
                alignment: Alignment.bottomLeft,
                child: Text(chatTxt, textAlign: TextAlign.left, style: const TextStyle(fontWeight: FontWeight.w400),),
              )
            ],
          ),
          decoration: BoxDecoration(
            color: color,
            borderRadius: borderRadius
          ),
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SliceChatViewModel>.reactive(
      viewModelBuilder: () => SliceChatViewModel(),
      onModelReady: (model) => model.initialize(context),
      builder:(context, model, child){
        return Scaffold(
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  gradient: AppConfig.BACKGROUND_CHAT_GRADIENT_COLOR,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: model.screenSize.height*0.83,
                  child: RefreshIndicator(
                    onRefresh: model.refreshChat,
                    child: ListView.builder(
                      itemCount: model.listChat.length,
                      itemBuilder: (BuildContext context, int index){
                        return buildChatElementWidget(model, context, index);
                      },
                    ),
                  ),
                )
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  color: Colors.pink,
                  height: model.screenSize.height*0.12,
                )
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  constraints: BoxConstraints(maxHeight: model.screenSize.height*0.5),
                  padding: const EdgeInsets.all(5),
                  alignment: Alignment.center,
                  height: model.screenSize.height*0.08,
                  color: Colors.blue.shade800,
                  child: Row(
                    children: [
                        Container(
                          constraints: BoxConstraints(maxHeight: model.screenSize.height*0.5),
                          width: model.screenSize.width*0.81,
                          height: model.screenSize.height*0.08,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(100))
                          ),
                          child: Stack(
                            children: [
                              TextField(
                                cursorHeight: model.screenSize.height*0.035,
                                textAlign: TextAlign.start,
                                keyboardType: TextInputType.multiline,
                                controller: model.inputMessageController,
                                style: TextStyle(fontSize: model.screenSize.height*0.025),
                                maxLines: null,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 20, right: 10, top: 25),
                                  isDense: true,   
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                ),
                              ),
                            ],
                          )
                        ),
                   
                    ],
                  ),
                ),
              ),
              CircularMenu(
                alignment: Alignment.bottomRight,
                toggleButtonSize: model.screenSize.width*0.081,
                endingAngleInRadian: 4.7,
                startingAngleInRadian : 3.2,
                curve: Curves.bounceOut,
                items: [
                  CircularMenuItem(icon: Icons.settings_voice_rounded, onTap: () {
                    // callback
                  }),
                  CircularMenuItem(icon: Icons.attach_file_outlined, onTap: model.pickFile),
                  CircularMenuItem(icon: Icons.camera_alt_sharp, onTap: () {
                    //callback
                  }),
                ]
              )
            ],
          )
        );
      },
    );
  }
}