import 'dart:convert';
import 'dart:io';

import 'package:app/src/core/app_config.dart';
import 'package:app/src/core/container.dart' as appContainer;
import 'package:app/src/grpc/storage_service/ChatService.pb.dart';
import 'package:app/src/helper/signal_r_helper.dart';
import 'package:app/src/model/ChatRoomModel.dart';
import 'package:app/src/model/UserModel.dart';
import 'package:app/src/screen/slice_chat_optional_form_screen.dart';
import 'package:file_manager/file_manager.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked/stacked.dart';
import 'package:app/src/core/container.dart' as appContainer;

class SliceChatRoomViewModel extends BaseViewModel{
    late BuildContext _context;
    late Size _screenSize;
    final ScrollController _chatScrollController = ScrollController();
    late SignalRHelper _signalRHelper;
    late ChatRoomModel _dataRoom;
    late SharedPreferences _localStorage;
    List<Map<String, String>> _listChat = [];
    UserModel _userData = UserModel();
    TextEditingController _inputMessageController = new TextEditingController();

    Size get screenSize => _screenSize;
    ScrollController get chatScrollController => _chatScrollController;
    List<Map<String, String>> get listChat => _listChat;
    TextEditingController get inputMessageController => _inputMessageController;
    ChatRoomModel get dataRoom => _dataRoom;
    UserModel get userData => _userData;

    initialize(BuildContext context, ChatRoomModel dataRoom) async{
      setBusy(true);
      notifyListeners();
      try{
       
        _dataRoom = dataRoom;
        _context = context;
        _screenSize = MediaQuery.of(_context).size;
        _signalRHelper = SignalRHelper(AppConfig.SIGNAL_R_SERVER_ADDRESS+"/hub/chat?x-app-key="+AppConfig.GRPC_REALTIME_SERVICE_APP_KEY);

        _localStorage = await SharedPreferences.getInstance();
        String? userJson = _localStorage.getString("data_user_login");
        if( userJson != null){
          _userData.fromJson(userJson);
        }

        _signalRHelper.on("joinedRoom", (payload){
            setBusy(false);
            var data = payload![0];
            Map<String,String> dataChat = {
              "text":data.toString(),
              "type":"info"
            };
            _listChat.add(dataChat);
            notifyListeners();
        });
        _signalRHelper.on("receiveRoomMessage", (data){
            Map<String,String> dataChat = {
              "text":data![1].toString(),
              "type":"chat",
              "userId":data![0].toString()
            };
            _listChat.add(dataChat);
            notifyListeners();
        });
        _signalRHelper.onConnect((clientId) async {
          await _signalRHelper.send("joinChatRoom", args: [_dataRoom.id, clientId, _userData.email, _dataRoom.name]);
        });
        await _signalRHelper.connect();
        
        
      }catch(e){
        print(e);
          Navigator.push(_context, 
            MaterialPageRoute(builder: (context) => SliceChatOptionalFormScreen())
          );
      }
      
      notifyListeners();
    }

    Future<void> refreshChat() async {
      print("------ refresh chat ---------------");
      Map<String,String> dataChat = {
        "text":"ewfresh"+_listChat.length.toString(),
        "type":"chat"
      };
      _listChat.add(dataChat);
      notifyListeners();
    }

    pickFile() async {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          allowMultiple: true
      );
                    
      if(result != null){
        File file = File(result.files.single.path!);
        await uploadFile(file);
      }
    }

    uploadFile(File file) async {
      ResponseFuture<UploadFileResponse> res = await appContainer.Container.chatService.uploadFile(file);
      print(res);
    }

    sendMessage() async {
      var response = await appContainer.Container.realtimeChatService.sendMessage(_dataRoom.id, _signalRHelper.clientId, _userData.id, _inputMessageController.text);
      if(!response.success){
        print(response.errorMessage);
      }
    }
}