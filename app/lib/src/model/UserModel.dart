import 'dart:convert';

class UserModel {
  String email;
  String id;
  String username;
  String password;

  UserModel({this.id = "", this.email = "", this.username = "", this.password = ""}){
    // id = id;
    // email = email;
    // username = username;
    // password = password;
  }

  fromJson(String json){
    Map<String, dynamic> dataUserLogin = jsonDecode(json);

    email = dataUserLogin.containsKey("email") ? dataUserLogin["email"] : "";
    username = dataUserLogin.containsKey("username") ? dataUserLogin["username"] : "";
    id = dataUserLogin.containsKey("id") ? dataUserLogin["id"] : "";
    password = dataUserLogin.containsKey("password") ? dataUserLogin["password"] : "";
  }

  String toJson(){
    Map<String, dynamic> data = {
      "id": id,
      "email": email,
      "password": password,
      "username": username
    };

    return JsonEncoder().convert(data);
  }

}