import 'dart:io';

import 'package:app/src/grpc/storage_service/ChatService.pbgrpc.dart';
import 'package:app/src/core/container.dart' as appContainer;
import 'package:file_manager/file_manager.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';
import 'package:stacked/stacked.dart';

class SliceChatViewModel extends BaseViewModel{
    late BuildContext _context;
    late Size _screenSize;
    final ScrollController _chatScrollController = ScrollController();
    List<String> _listChat = [];
    TextEditingController _inputMessageController = new TextEditingController();

    Size get screenSize => _screenSize;
    ScrollController get chatScrollController => _chatScrollController;
    List<String> get listChat => _listChat;
    TextEditingController get inputMessageController => _inputMessageController;

    initialize(BuildContext context) async{
      _context = context;
      _screenSize = MediaQuery.of(_context).size;

      for(var i = 0; i < 100; i++){
        _listChat.add(List.filled(i+1, "asd").join());
      }

      notifyListeners();
    }

    Future<void> refreshChat() async {
      print("------ refresh chat ---------------");
      _listChat.insert(0, "messsage from refresh");
      notifyListeners();
    }

    pickFile() async {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          allowMultiple: true
      );
                    
      if(result != null){
        File file = File(result.files.single.path!);
        await uploadFile(file);
      }
    }

    uploadFile(File file) async {
      ResponseFuture<UploadFileResponse> res = await appContainer.Container.chatService.uploadFile(file);
      print(res);
    }
}