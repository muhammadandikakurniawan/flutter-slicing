import 'dart:io';
import 'dart:async';
import 'package:app/src/grpc/storage_service/ChatService.pbgrpc.dart';
import 'package:app/src/core/app_config.dart';
import 'package:chunked_stream/chunked_stream.dart';
import 'package:grpc/grpc.dart';
import 'package:path/path.dart';

class ChatService {

  late ChatServiceClient _grpcClient;
  late ClientChannel _channel;

  ChatService(){
    _channel = ClientChannel(AppConfig.GRPC_STORAGE_SERVICE_HOST,port: AppConfig.GRPC_STORAGE_SERVICE_PORT,options: const ChannelOptions(connectionTimeout: Duration(seconds: AppConfig.GRPC_STORAGE_SERVICE_TIMEOUT),credentials: ChannelCredentials.insecure()));

    Map<String, String> meta = {
      "x-api-key": AppConfig.GRPC_STORAGE_SERVICE_APP_KEY
    };

    _grpcClient = ChatServiceClient(_channel, options: CallOptions(metadata: meta));
  }


    Future<ResponseFuture<UploadFileResponse>> uploadFile(File file) async{

      Stream<UploadFileRequest> generateRequest() async*{
        
        UploadFileRequest request = UploadFileRequest();

        String extention = file.path.split(".").last;

        UploadFileRequest_FileInfo fileInfo = UploadFileRequest_FileInfo(
          bucket: "user-1",
          fileExtention: extention,
          fileName: basename(file.path),
          fileType: extention
        );
        request.fileInfo = fileInfo;
        yield request;

        final reader = ChunkedStreamIterator(file.openRead());
        while(true){
          var data = await reader.read(4096);
          if(data.length <= 0){
            break;
          }
          UploadFileRequest fileReq = UploadFileRequest();
          fileReq.data = data;
          yield fileReq;
        }

      }

      ResponseFuture<UploadFileResponse> response = _grpcClient.uploadFile(generateRequest());
      return response;
    }

}