import 'dart:convert';
import 'package:app/src/core/app_config.dart';
import 'package:app/src/grpc/realtime_service/ChatService.pb.dart';
import 'package:app/src/helper/signal_r_helper.dart';
import 'package:app/src/model/ChatRoomModel.dart' as model;
import 'package:app/src/screen/slice_chat_room_screen.dart';
import 'package:flutter/material.dart';
import 'package:app/src/core/container.dart';
import 'package:app/src/model/UserModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked/stacked.dart';
import 'package:app/src/core/container.dart' as appContainer;

class SliceChatOptionalFormViewModel extends BaseViewModel {

  late BuildContext _context;
  late Size _screenSize;
  late SharedPreferences _localStorage;
  late Animation<double> _animation; 
  late AnimationController _controller; 
  late SignalRHelper _signalRHelper;
  UserModel _userData = UserModel();
  TextEditingController _inputRoomNameController = new TextEditingController();

  Size get screenSize => _screenSize;
  TextEditingController get inputRoomNameController => _inputRoomNameController;
  UserModel get userData => _userData;
  Animation<double> get animation => _animation; 
  AnimationController get controller => _controller; 
  BuildContext get context => _context;

  initialize(BuildContext context) async{
    _context = context;
    _screenSize = MediaQuery.of(_context).size;
    _localStorage = await SharedPreferences.getInstance();

    String? userJson = _localStorage.getString("data_user_login");
    if( userJson != null){
      _userData.fromJson(userJson);
    }

    notifyListeners();
  }

  createChatRoom() async {
    String roomName =_inputRoomNameController.text;
    String userId = _userData.id;

    CreateRoomResponse response = await appContainer.Container.realtimeChatService.createRoom(roomName, userId);

    switch(response.statusCode){
      case "500":
        return;
        break;
      case "200":
        if(response.data != null){
          model.ChatRoomModel dataRoom = new model.ChatRoomModel(
            id: response.data.id,
            code: response.data.code,
            name: response.data.name,
            creatorUserId: response.data.creatorUserId,
            membersUserId: []
          );

          Navigator.push(_context, 
            MaterialPageRoute(builder: (context) => SliceChatRoomScreen(roomData: dataRoom,))
          );
        }
    }

   


    _inputRoomNameController.clear();
    notifyListeners();
  }

  joinRoom() async{

    String roomCode =_inputRoomNameController.text;
    String userId = _userData.id;

    JoinRoomResponse response = await appContainer.Container.realtimeChatService.joinRoom(roomCode, userId);
    switch(response.statusCode){
      case "500":
        return;
        break;
      case "200":
        if(response.data != null){
          model.ChatRoomModel dataRoom = new model.ChatRoomModel(
            id: response.data.id,
            code: response.data.code,
            name: response.data.name,
            creatorUserId: response.data.creatorUserId,
            membersUserId: []
          );

          Navigator.push(_context, 
            MaterialPageRoute(builder: (context) => SliceChatRoomScreen(roomData: dataRoom,))
          );
        }
    }
  }

}