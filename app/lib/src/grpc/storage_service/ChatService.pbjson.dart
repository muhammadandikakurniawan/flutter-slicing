///
//  Generated code. Do not modify.
//  source: ChatService.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use uploadFileRequestDescriptor instead')
const UploadFileRequest$json = const {
  '1': 'UploadFileRequest',
  '2': const [
    const {'1': 'fileInfo', '3': 1, '4': 1, '5': 11, '6': '.chat_service.UploadFileRequest.FileInfo', '10': 'fileInfo'},
    const {'1': 'data', '3': 2, '4': 1, '5': 12, '10': 'data'},
  ],
  '3': const [UploadFileRequest_FileInfo$json],
};

@$core.Deprecated('Use uploadFileRequestDescriptor instead')
const UploadFileRequest_FileInfo$json = const {
  '1': 'FileInfo',
  '2': const [
    const {'1': 'fileType', '3': 1, '4': 1, '5': 9, '10': 'fileType'},
    const {'1': 'fileName', '3': 2, '4': 1, '5': 9, '10': 'fileName'},
    const {'1': 'fileExtention', '3': 3, '4': 1, '5': 9, '10': 'fileExtention'},
    const {'1': 'bucket', '3': 4, '4': 1, '5': 9, '10': 'bucket'},
  ],
};

/// Descriptor for `UploadFileRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List uploadFileRequestDescriptor = $convert.base64Decode('ChFVcGxvYWRGaWxlUmVxdWVzdBJECghmaWxlSW5mbxgBIAEoCzIoLmNoYXRfc2VydmljZS5VcGxvYWRGaWxlUmVxdWVzdC5GaWxlSW5mb1IIZmlsZUluZm8SEgoEZGF0YRgCIAEoDFIEZGF0YRqAAQoIRmlsZUluZm8SGgoIZmlsZVR5cGUYASABKAlSCGZpbGVUeXBlEhoKCGZpbGVOYW1lGAIgASgJUghmaWxlTmFtZRIkCg1maWxlRXh0ZW50aW9uGAMgASgJUg1maWxlRXh0ZW50aW9uEhYKBmJ1Y2tldBgEIAEoCVIGYnVja2V0');
@$core.Deprecated('Use uploadFileResponseDescriptor instead')
const UploadFileResponse$json = const {
  '1': 'UploadFileResponse',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `UploadFileResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List uploadFileResponseDescriptor = $convert.base64Decode('ChJVcGxvYWRGaWxlUmVzcG9uc2USFgoGc3RhdHVzGAEgASgJUgZzdGF0dXM=');
